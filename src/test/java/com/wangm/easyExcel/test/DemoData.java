package com.wangm.easyExcel.test;

import com.alibaba.excel.annotation.ExcelProperty;
import com.wangm.easyExcel.annotation.ExcelCheck;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author wangmeng
 * @since 2025/3/21
 */
@Data
public class DemoData {

    @ExcelProperty("订单号")
    @ExcelCheck(canRepeat = false)
    private String orderNo;
    @ExcelProperty("金额")
    @ExcelCheck(canEmpty = false)
    private BigDecimal amount;
    @ExcelProperty("用户")
    @ExcelCheck(length = 4)
    private String name;

}
