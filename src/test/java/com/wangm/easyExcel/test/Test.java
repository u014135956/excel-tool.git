package com.wangm.easyExcel.test;

import com.alibaba.excel.exception.ExcelDataConvertException;
import com.alibaba.fastjson.JSON;
import com.wangm.easyExcel.ExcelImportProcessor;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * @author wangmeng
 * @since 2025/3/21
 */
public class Test {

    public static void main(String[] args) throws URISyntaxException {
        String basePath = Test.class.getResource("/").getPath();
        if (basePath.startsWith("/") && basePath.charAt(2) == ':') {
            basePath = basePath.substring(1); // 去掉开头的 "/"
        }

        String filePath = basePath + "demo.xlsx";
        String errorFilePath = basePath + "error.xlsx";

        ExcelImportProcessor processor = new ExcelImportProcessor();

        try (InputStream outputStream = Files.newInputStream(Paths.get(filePath))) {
            List<DemoData> demoDataList = processor.importData(outputStream, DemoData.class);
            System.out.println(JSON.toJSONString(demoDataList));
        } catch (IOException e) {
        } catch (ExcelDataConvertException excelCheckException) {
            if (processor.generateErrorSheet(filePath, errorFilePath)) {
                System.out.println("输出错误文件到:" + errorFilePath);
            }
        }


    }


}
