package com.wangm.exception;

/**
 * excel导入异常
 *
 * @author wangmeng
 * @since 2023/5/29
 */
public class ExcelImportException extends Exception {

    public ExcelImportException() {
        super();
    }

    public ExcelImportException(String message) {
        super(message);
    }
}
