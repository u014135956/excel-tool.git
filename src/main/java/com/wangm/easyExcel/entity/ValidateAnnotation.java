package com.wangm.easyExcel.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.wangm.easyExcel.annotation.ExcelCheck;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 校验信息
 *
 * @author wangmeng
 * @since 2025/3/21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ValidateAnnotation {

    private ExcelProperty excelProperty;

    private ExcelCheck excelCheck;

    private String headName;

}
