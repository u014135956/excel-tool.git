package com.wangm.easyExcel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * excel导入错误信息
 *
 * @author wangmeng
 * @since 2024/5/25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class ExcelErrorMessage {

    /**
     * 行号
     */
    private Integer rowNum;


    /**
     * 错误信息
     */
    private String message;

}
