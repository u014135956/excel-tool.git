package com.wangm.easyExcel.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 导入校验注解
 *
 * @author wangmeng
 * @since 2024/5/25
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ExcelCheck {

    /**
     * 是否可以为空,默认是
     */
    boolean canEmpty() default true;

    /**
     * 是否可以重复,默认是
     */
    boolean canRepeat() default true;

    /**
     * 长度校验,只对String生效
     */
    int length() default -1;
}
