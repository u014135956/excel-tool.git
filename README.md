# excel-tool

基于easyExcel实现的导入功能封装,支持在导入时对数据进行非空、长度、重复性校验；
支持针对错误信息生成一份错误信息excel；

# 效果展示
![image.png](https://raw.gitcode.com/u014135956/excel-tool/attachment/uploads/1a164820-ba10-4e00-b32b-931787efd156/image.png 'image.png')

# 使用案例
通过注解配置校验规则

![image.png](https://raw.gitcode.com/u014135956/excel-tool/attachment/uploads/da585d29-158b-423c-bb78-7568a9c6d4d5/image.png 'image.png')

测试方法

![image.png](https://raw.gitcode.com/u014135956/excel-tool/attachment/uploads/6e79cfea-e92d-4cdd-815a-4038df35c16a/image.png 'image.png')